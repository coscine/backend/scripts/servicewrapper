# ServiceWrapper

Simple wrapper to start an arbitrary executable as a windows service.

## Getting started

Build the project with msbuild/Visual Studio 2017.

Use the sc command to add your service like so:
```
sc.exe create "YourServiceName" binPath="Path\to\ServiceWrapper.exe YourServiceName Path\to\your\custom\exe Arg1 Arg2 Arg3... " DisplayName="YourServiceName"
```
The arguments 
``
Arg1 Arg2 Arg3...
``
are optional and will be passed to your custom executable as commandline parameters.

As an example:
```
sc.exe create "Nomad" binPath="C:\Programs\ServiceWrapper\TestService.exe Nomad C:\Programs\Nomad\Nomad.exe agent -config=C:\Programs\Nomad\server.conf" DisplayName="Nomad"
```
creates a service for the application Nomad and passes its configuration to it.

Erro logs are located in you temp folder in ``ServiceWrapper.log``.