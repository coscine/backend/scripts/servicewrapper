﻿using System.ServiceProcess;

namespace Coscine.ServiceWrapper
{
    class CustomService : ServiceBase
    {
        string ExecutablePath { get; set; }
        string[] CustomArgs {get; set;}

        public CustomService(string serviceName, string executablePath, string[] customArgs)
        {
            ServiceName = serviceName;
            ExecutablePath = executablePath;
            CustomArgs = customArgs;
        }

        protected override void OnStart(string[] args)
        {
            RunnableWrapper.Instance.Start(CustomArgs, ExecutablePath);
        }

        protected override void OnStop()
        {
            RunnableWrapper.Instance.Stop();
        }

        protected override void OnShutdown()
        {
            RunnableWrapper.Instance.Stop();
        }
    }
}
