﻿using System;
using System.Linq;
using System.ServiceProcess;

namespace Coscine.ServiceWrapper
{
    class Program
    {
        /* Expect the service name as first argument,
         * the path to the executable (which is to be wrapped) as second argument
         * and the rest of the arguments as arguments for the executable (not mandatory).
         */
        static void Main(string[] args)
        {
            string name = args[0];
            string executablePath = args[1];

            var customArgs = args.Skip(2).ToArray();

            // running as console app?
            if (Environment.UserInteractive)
            {
                RunnableWrapper.Instance.Start(customArgs, executablePath);

                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);

                RunnableWrapper.Instance.Stop();
            }
            else
            {
                using (var service = new CustomService(name, executablePath, customArgs))
                {
                    ServiceBase.Run(service);
                }
            }
        }
    }
}